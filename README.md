# Paasify

Deploy docker-compose files with ease!

Paasify is a little tool that will help deploying `docker-compose.yml` based apps, either on docker swarm or on regular docker (via docker-compose). It will also manage the different git repositories you need to build your infrastructure. This project try to overstep the missing gap between the docker-compose deployment easiness and infrastructure as a code. Also, this project propose an opiniated devops workflow.


## Quickstart

### Installation

Tou can either clone this repository, or install the soruce file directly:
```
# Install paasify directly
curl -o /usr/local/bin/paasify https://raw.githubusercontent.com/mrjk/paasify/master/paasify && sudo chmod -x /usr/local/bin/paasify

# Install from sources
git clone https://github.com/mrjk/paasify.git
cd paasify
sudo ln -s $PWD/paasify /usr/local/bin/paasify
```

Then you can test the help message to check if everything is good:
```
paasify help
```


### Examples TODO

Now, you can go to the example directory and deploy your first stack. It can takes some time for the first time, especially because it will download the source image:
```
$ cd examples/demo1
$ paasify start
```
Behind the command, paasify actually went into all subdirectories and it ran `docker-compose -f docker-compose.yml -f docker-compose ... config` to generate a `docker-compose.run.yml` file.
This generated file will be used to start the stack, either with `docker-compose` or `docker stack`. You can check if your service is running with the following command:
```
# For docker
docker ps

# For Docker swarm
docker stack ls
docker service ls <STACK_NAME>
```
From there, you can access to this service locally, on the [http://127.0.0.1/8080](http://127.0.0.1/8080) URL. Now you've tested the service, you want to clean up the place, simply run:
```
paasify stop
```
And then, everything is gone. 


## Documentation

Documentation is available in `/docs` directory:
* [Usage](docs/usage.md)
* [Configuration](docs/configuration.md)
* [Workflow](docs/workflow.md)

## External resources

* Docker scurity best practices: https://www.smarthomebeginner.com/traefik-2-docker-tutorial/

## Informations

Meta:
* Author: Robin Cordier
* Date: 11/2021
* Status: Beta
* License: GPLv3

Kudos:
* [shawly/docker-templates](https://github.com/shawly/docker-templates) repo, where `paasify` is the missing piece.


