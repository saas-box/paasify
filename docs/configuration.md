# Paasify configuration

Paasify works with a configuration file. This configuration file is a simple shell var file, and it
must be called `paasify.conf.sh` in the root of your project directory.

## Options

Breakdown of configuration options.

### Project bases

This is the most important configuration:
```
PRJ_NS=adm
PRJ_DOMAIN=mydomain
PRJ_NETWORK=mydockernet
```

Notes:
* Project NS is actually a prefix, all directories starting by this prefix will be autoloaded
* Domain is the top domain to use.
* Network is the name of the docker network to use for this stack

### Sub projects

Each stack is a three field setup:
* Field 1:
  * Local directory of the stack
  * Required and must be unique
* Field 2:
  * Remote git URL if it's a repo
  * Can be a read-only URL or a read-write for your own stacks
  * Optional
* Field 3:
  * Remote git branch to checkout if it's a repo
  * Optional

Order of stack is important, as paasify will create stack one by ones, starting by the first. When paasify destroy things, it will process this list in the reverse order. Empty lines and lines starting by `#` are ignored.

Configuration example:
```
PRJ_STACKS="
# Stack_Name  Git_URL Git_Branch
infra            git@framagit.org:mrjk-docker-compose/infra.git  main
infra/portainer                                                  
utils            git@framagit.org:mrjk-docker-compose/utils.git  main
cloud/gaia
"
```

### Project tags

Project Tags are a smart way to include parts of docker-compose. By default, only tags of the current namespace
are loaded. There is one basic configuration and one more customizable.

#### Simple tags
This is a list of tags to add/read by default:
```
PRJ_TAGS_FILTER=
PRJ_TAGS_SUFFIX=
PRJ_TAGS_PREFIX=box
```
Please note that the namespace name is also a tag.

### Advanced tags

The tag system allows you to apply template rules on some projects, in a fine grained way. The first thing to understand is the fact this is a table. The fields are:
* `Stack`: represent a stack, use `*` as wildcard
* `variable`: represent a list of tempalte to read. The special char `-` or value `final` means that no rules will be executed after this one.
* `patterns`: represent the patterns to match. The `*` is a wildcard, and many patterns can be written. To reference another variable PREVIOUSLY declared, use the `@` sign (ie: `@myvar`)

Some important points to note:
* The table is read from top to bottom. You can't use variable befopre they has been previsouly declared.
* This configuration must be set in single quotes `'`, otherwise the shell will try to interpret the value of regular variable (TOFIX)
* You can use your editor to keep the table clean ;)


It is possible to customize wich tag on which stack is run, via the `PRJ_TAGS` directive.
Empty lines and lines starting by `#` are ignored. Example:
```
PRJ_TAGS="
# Stacks  Var  Tag_list
*   dev   wks1,internal,box
*   -   traefik,dev
"
```

To debug projects tags, your can check the config map:
```
$ paasify context
[...]
NOTICE: ================ CONFIG MAP
NOTICE: Stack    Var     Value
NOTICE: ---      ---     ---
NOTICE: *        prefix  -
NOTICE: *        main    barbu
NOTICE: *        suffix  user*,override
NOTICE: erpnext  prefix  erpnext,redis,mysql
NOTICE: traefik  -       $prefix,$main,$suffix
NOTICE: erpnext  -       $prefix,$main,$suffix
NOTICE: kimai    -       $prefix,$main,$suffix
NOTICE: barbu*   -       $prefix,$main,$suffix  #Optional

```

To understand the processing workflow:
* Paasify will read each line top to down
* Paasify stop to process when it meet a `-`
  * 


### Sub stacks
It is possible to nest Paasify project. Simply ensure the presence of a `paasify.conf.sh` file within the concerned sub-folder.

    > Note: THis feature is in beta


### Deprecated
```
PRJ_NETWORK_CREATE=false
PRJ_NETWORK_OPTS="--subnet 10.1.1.0/24 --ip-range=10.1.1.224/27 --gateway=10.1.1.1"
```

