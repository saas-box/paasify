# Paasify Workflows

We describes here workflow and best practices for paasify.


## Workflow

The workflow is pretty simple:
* Create a `paasify.conf.sh` file
    * Describe your project
    * Describes which stacks you want to use
* Add stacks:
    * You can:
        * Clone an existing stack repo, it must have a `docker-compose.yml` file in it. Good for reusable stacks.
        * Create a new stack directly into your project, with a `docker-compose.yml`. Good for specific/non-reusable stacks.
        * Clone a sub-project, where there is a `paasify.conf.sh` file in it.
* Update your stack config and the tags you want to build into the top `paasify.conf.sh`
* Build and deploy your stacks:
  * Build compose files: `paasify build`
  * Run compose files: `paasify up`
  * Rebuild the whole and redeploy: `paasify restart`
* If you modified files:
  * In the top project:
      * Simply git your files
  * In other repos:
      * If specific to your project, simply git add the file
      * If generic, use the `paasify git` wrapper to commit in upstream projects
  * To update all remote repos: `paasify update`


## Best practices

Here we will cover the paasify best practices.

### Stack project structure

Project structure:
```
prj1/
 | infra_traefik
 | infra_coredns
prj2/
 | code_
```
TODO

### Git repository dependencies

In your project, you defined different stacks. If you want to go trough Infrastructure as Code, you may want to have some souplesse to manage different stack from various sources. From this statement, there are two situations:
* One monolitique project, 
  * Stack templates are commited along the actual configuration.
  * This is perfectly fine for small scale project
  * Also fine for very customized projects
* Root repo and sub stacks
  * There is one root repository, where all your business configuration lives in
  * Your sub-stacks can either be a reference to other git repo, or be directly commited into the root repository.
  * This modele implies to have at least one stack repository

Either the first model is simpler for people less comfortable with git, either the professional will want to go with the second solution, which is more scalable.


### Multiple git repositories

There is a trick, with some clever tricks, it is possible to stack repositories. Paasify will deal with all of these subtitles to let you have a smooth experience with this. For a technical explanation, `paasify` will simply create a git repositoy at the top of your project, and every sub-stacks wich are also git repo are stored into a `.git-stack` dir instead of the regular `.git` dir. The management of ignored files is managed by `paasify`, as it will merge ignore list files from `paasify.conf.sh` and the shildren of currently cloned repositories.


TODO: Blind mode
The blind mode is the default mode, because it is simpler to use and it avoid to deal with complex git situations. This mode is specially recommanded if you don't need to publish your stack on seprate git repos.
* Enabled:
  * Root dir: Root repo
  * Sub stack: Root repo, use paasify git to edit your stack
* Disabled:
  * Root dir: Root repo
  * Sub dir: Sub repo


### Tips and tricks

* When you are not in the root project, paasify will only work on the stack you are in.
  * TOFIX: There is a bug that trigger the whole stack action when in a non managed su-dir
* Debug logs are pretty helpfull
* Paasify was designed in mind to be automated


