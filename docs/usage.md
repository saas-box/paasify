# Usage


## Paasify usage

Paasify is a context aware command line. When it starts, it will determine project root dir and consider it as its working directory. Paasify understand sub-directories, if a paasify command is run from a sub directory, it will only work for the given stack. It is quite useful during development phase.

## Command line options

There is the paasify help message:
```
$ paasify help
paasify is a tool to assemble and deploy docker-compose files.

usage: paasify [<OPTIONS>] <COMMAND> [<ARGS>]
       paasify help

OPTIONS:
  -n         Dry mode
  -v         Verbose mode
  -d         Debug mode
  -f         Force mode

COMMANDS:
  build      Build templates
  context    Show current context
  git        Wrap git command
  help       Show this help
  list       List all stasks
  logs       Show logs
  net        Start or stop project network
  ps         Show process
  restart    Restart and rebuild stacks
  start      Start stacks
  stop       Stop stacks
  update     Update all git repositories

workflow:
  update -> build -> run -> restart -> stop

info:
  author: mrjk.78 <mrjkDOT78ATgmailDOTcom>
  version: 0.0.4-beta (2021-12-02)
  license: GPLv3

```

## Troubleshooting

Show stacks:
```
paasify list
```

Show the logs:
```
paasify logs
paasify logs -f
```

Show process:
```
paasify ps
```

You can run at anytime debug mode and dry-mode:
```
# Run paasify in dry mode, it will not modify files and show commands it executes
paasify -n

# Get Debug logs
paasify -d

# Force paasify to process undeclared stacks:
paasify -f
```
